package com.craig_38.mcoverlays;

import java.io.File;

import com.craig_38.mcoverlays.configuration.ConfigurationHandler;
import com.craig_38.mcoverlays.handler.OverlayHandler;
import com.craig_38.mcoverlays.lib.Reference;
import com.craig_38.mcoverlays.overlay.Overlays;
import com.craig_38.mcoverlays.proxy.CommonProxy;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION)
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class MCOverlays
{
    @Instance(Reference.MOD_ID)
    public static MCOverlays instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY, serverSide = Reference.COMMON_PROXY)
    public static CommonProxy proxy;
    
    public OverlayHandler overlayHandler; //Defer instantiating the handler until the configs are loaded, or there will be null pointers;
        
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
		ConfigurationHandler.init(new File(event.getModConfigurationDirectory().getAbsolutePath() + File.separator + Reference.MOD_ID + File.separator + Reference.MOD_ID + ".cfg"));
		overlayHandler = new OverlayHandler();
        proxy.registerRenderTickHandler();
        
        Overlays.init();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {

    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {

    }
   
}
