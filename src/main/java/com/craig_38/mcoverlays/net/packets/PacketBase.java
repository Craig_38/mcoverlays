package com.craig_38.mcoverlays.net.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.craig_38.mcoverlays.lib.Reference;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public abstract class PacketBase
{
    public abstract int packetId();

    public abstract void writeData(DataOutputStream stream) throws IOException;

    public abstract void readData(DataInputStream stream) throws IOException;

    /**
     * Execute packet when received on the client-side.
     * 
     * @param player
     *            Reference to the local player
     */
    public abstract void executeClient(EntityPlayer player);

    /**
     * Execute packet when received on the server-side.
     * 
     * @param player
     *            The player that sent the packet.
     */
    public abstract void executeServer(EntityPlayer player);

    public final Packet getPacket()
    {
        final Packet250CustomPayload packet = new Packet250CustomPayload();
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        final DataOutputStream dstream = new DataOutputStream(stream);

        try
        {
            dstream.writeInt(packetId());
            writeData(dstream);

            packet.channel = Reference.NETWORK_CHANNEL;
            packet.isChunkDataPacket = false;
            packet.data = stream.toByteArray();
            packet.length = stream.size();

            return packet;
        }
        catch (final Exception e)
        {
            return null;
        }
    }
}
