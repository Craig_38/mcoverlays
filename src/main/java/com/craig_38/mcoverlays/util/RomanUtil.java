package com.craig_38.mcoverlays.util;

public final class RomanUtil
{
    private RomanUtil()
    {
        
    }
    
    private enum RomanNumerals
    {
        M(1000),
        CM(900),
        D(500),
        CD(400),
        C(100),
        XC(90),
        L(50),
        XL(40),
        X(10),
        IX(9),
        V(5),
        IV(4),
        I(1);
        
        public int value;
        
        private RomanNumerals(int value)
        {
            this.value = value;
        }
    }
    
    public static String getRomanNumeralString(int number)
    {
        StringBuilder roman = new StringBuilder();
        
        for (RomanNumerals numeral : RomanNumerals.values())
        {
            while (number >= numeral.value)
            {
                number -= numeral.value;
                roman.append(numeral.name());
            }
        }
        
        return roman.toString();
    }
}
