package com.craig_38.mcoverlays.util;

public class ExtraMath
{
	public static double lerp(double a, double b, double t)
	{
	    return a + (b - a) * t;
	}
}
