package com.craig_38.mcoverlays.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import cpw.mods.fml.client.FMLClientHandler;

public class RendererUtil
{
	public static void Enable2DMode()
	{
		final Minecraft minecraft = FMLClientHandler.instance().getClient();
		ScaledResolution scaledresolution = new ScaledResolution(minecraft.gameSettings, minecraft.displayWidth, minecraft.displayHeight);
		
        GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, scaledresolution.getScaledWidth(), scaledresolution.getScaledHeight(), 0.0D, 0, 3000.0D);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
	}
	
    /**
     * Draws a textured rectangle at the stored z-value. Args: x, y, u, v, width, height, zLevel, imageWidth, imageHeight, alpha
     */
    public static void drawAdvancedTexturedModalRect(int x, int y, int u, int v, float width, float height, double zLevel, int imageWidth, int imageHeight, int alpha)
    {
    	GL11.glTexEnvi(GL11.GL_TEXTURE_ENV, GL11.GL_TEXTURE_ENV_MODE, GL11.GL_MODULATE);
        float f = (float) 1 / imageWidth;
        float f1 = (float) 1 / imageHeight;
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, alpha);
        tessellator.addVertexWithUV((double)(x + 0), (double)(y + height), zLevel, (double)((float)(u + 0) * f), (double)((float)(v + height) * f1));
        tessellator.addVertexWithUV((double)(x + width), (double)(y + height), zLevel, (double)((float)(u + width) * f), (double)((float)(v + height) * f1));
        tessellator.addVertexWithUV((double)(x + width), (double)(y + 0), zLevel, (double)((float)(u + width) * f), (double)((float)(v + 0) * f1));
        tessellator.addVertexWithUV((double)(x + 0), (double)(y + 0), zLevel, (double)((float)(u + 0) * f), (double)((float)(v + 0) * f1));
        tessellator.draw();
    }
    
    public static void drawDynamicBox(int x, int y, int width, int height, int imageSize, int cornerSize, int zLevel, ResourceLocation background)
    {
    	drawDynamicBox(x, y, width, height, imageSize, cornerSize, zLevel, background, 255);
    }
    
    public static void drawDynamicBox(int x, int y, int width, int height, int imageSize, int cornerSize, int zLevel, ResourceLocation background, int alpha)
    {
    	Minecraft minecraft = FMLClientHandler.instance().getClient();
    	
    	int right = x + width;
    	int bottom = y + height;
    	
    	float uvCorner = (cornerSize / (float)imageSize);
    	    	
    	minecraft.func_110434_K().func_110577_a(background);
    	GL11.glTexEnvi(GL11.GL_TEXTURE_ENV, GL11.GL_TEXTURE_ENV_MODE, GL11.GL_MODULATE);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, alpha);
        tessellator.addVertexWithUV(x, y + cornerSize, zLevel, 0, uvCorner);
        tessellator.addVertexWithUV(x + cornerSize, y + cornerSize, zLevel, uvCorner, uvCorner);
        tessellator.addVertexWithUV(x + cornerSize, y, zLevel, uvCorner, 0);
        tessellator.addVertexWithUV(x, y, zLevel, 0, 0);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(right - cornerSize, y + cornerSize, zLevel, 1 - uvCorner, uvCorner);
        tessellator.addVertexWithUV(right, y + cornerSize, zLevel, 1, uvCorner);
        tessellator.addVertexWithUV(right, y, zLevel, 1, 0);
        tessellator.addVertexWithUV(right - cornerSize, y, zLevel, 1- uvCorner, 0);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(right, bottom - cornerSize, zLevel, 1, 1 - uvCorner);
        tessellator.addVertexWithUV(right - cornerSize, bottom - cornerSize, zLevel, 1 - uvCorner, 1 - uvCorner);
        tessellator.addVertexWithUV(right - cornerSize, bottom, zLevel, 1 - uvCorner, 1);
        tessellator.addVertexWithUV(right, bottom, zLevel, 1, 1);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(x, bottom, zLevel, 0, 1);
        tessellator.addVertexWithUV(x + cornerSize, bottom, zLevel, uvCorner, 1);
        tessellator.addVertexWithUV(x + cornerSize, bottom - cornerSize, zLevel, uvCorner, 1 - uvCorner);
        tessellator.addVertexWithUV(x, bottom - cornerSize, zLevel, 0, 1- uvCorner);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(x + cornerSize, y, zLevel, uvCorner, 0);
        tessellator.addVertexWithUV(x + cornerSize, y + cornerSize, zLevel, uvCorner, uvCorner);
        tessellator.addVertexWithUV(right - cornerSize, y + cornerSize, zLevel, 1 - uvCorner, uvCorner);
        tessellator.addVertexWithUV(right - cornerSize, y, zLevel, 1 - uvCorner, 0);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(x, y + cornerSize, zLevel, 0, uvCorner);
        tessellator.addVertexWithUV(x, bottom - cornerSize, zLevel, 0, 1 - uvCorner);
        tessellator.addVertexWithUV(x + cornerSize, bottom - cornerSize, zLevel, uvCorner, 1 - uvCorner);
        tessellator.addVertexWithUV(x + cornerSize, y + cornerSize, zLevel, uvCorner, uvCorner);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(x + cornerSize, bottom - cornerSize, zLevel, uvCorner, 1 - uvCorner);
        tessellator.addVertexWithUV(x + cornerSize, bottom, zLevel, uvCorner, 1);
        tessellator.addVertexWithUV(right - cornerSize, bottom, zLevel, 1 - uvCorner, 1);
        tessellator.addVertexWithUV(right - cornerSize, bottom - cornerSize, zLevel, 1- uvCorner, 1 - uvCorner);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(right - cornerSize, y + cornerSize, zLevel, 1 - uvCorner, uvCorner);
        tessellator.addVertexWithUV(right - cornerSize, bottom - cornerSize, zLevel, 1 - uvCorner, 1 - uvCorner);
        tessellator.addVertexWithUV(right, bottom - cornerSize, zLevel, 1, 1 - uvCorner);
        tessellator.addVertexWithUV(right, y + cornerSize, zLevel, 1, uvCorner);
        tessellator.draw();
        
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 255);
        tessellator.addVertexWithUV(x + cornerSize, y + cornerSize, zLevel, uvCorner, uvCorner);
        tessellator.addVertexWithUV(x + cornerSize, bottom - cornerSize, zLevel, uvCorner, 1 - uvCorner);
        tessellator.addVertexWithUV(right - cornerSize, bottom - cornerSize, zLevel, 1 - uvCorner, 1 - uvCorner);
        tessellator.addVertexWithUV(right - cornerSize, y + cornerSize, zLevel, 1 - uvCorner, uvCorner);
        tessellator.draw();
    }
}
