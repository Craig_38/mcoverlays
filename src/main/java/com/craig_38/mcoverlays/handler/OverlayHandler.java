package com.craig_38.mcoverlays.handler;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.lwjgl.input.Keyboard;

import com.craig_38.mcoverlays.overlay.Overlay;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class OverlayHandler implements ITickHandler
{
	private List<Overlay> overlayList = new ArrayList<Overlay>();
	
	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData)
	{
		
	}
	
	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData)
	{
		if (type.contains(TickType.CLIENT))
		{
			for (Overlay overlay : overlayList)
			{
				if (overlay.active)
				{
					handleKeyboardInput(overlay);
				}
			}
		}
		
		if (type.contains(TickType.RENDER))
		{
			for (Overlay overlay : overlayList)
			{
				if (overlay.active)
				{
					overlay.UpdateVariables();
					overlay.Render();
				}
			}
		}
	}

	@Override
	public EnumSet<TickType> ticks()
	{
		return EnumSet.of(TickType.RENDER, TickType.CLIENT);
	}

	@Override
	public String getLabel()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	public void RegisterOverlay(Overlay overlay)
	{
		for (Overlay registeredOverlay : overlayList)
		{
			if (registeredOverlay.GetName().equals(overlay.GetName()))
			{
				throw new IllegalArgumentException("A mod attempted to register an overlay with a duplicate name: " + overlay.GetName());
			}
		}
		overlayList.add(overlay);
	}
	
	public List<Overlay> GetAllOverlays()
	{
		return this.overlayList;
	}
	
	public List<Overlay> GetActiveOverlays()
	{
		List<Overlay> activeOverlays = new ArrayList<Overlay>();
		
		for(Overlay overlay : overlayList)
		{
			if (overlay.active)
			{
				activeOverlays.add(overlay);
			}
		}
		
		return activeOverlays;
	}
	
    public void handleKeyboardInput(Overlay overlay)
    {
        if (Keyboard.getEventKeyState())
        {
            char character = Keyboard.getEventCharacter();
            int keyCode = Keyboard.getEventKey();
            
            overlay.KeyTyped(character, keyCode);
        }
    }
}
