package com.craig_38.mcoverlays.configuration;

import static net.minecraftforge.common.Configuration.CATEGORY_GENERAL;

import java.io.File;
import java.util.logging.Level;

import com.craig_38.mcoverlays.lib.Reference;

import cpw.mods.fml.common.FMLLog;

import net.minecraftforge.common.Configuration;

public class ConfigurationHandler
{
	public static Configuration configuration;
	
    public static final String CATEGORY_AESTHETICS = "Aesthetics";

	public static void init(File configFile)
	{
    	configuration = new Configuration(configFile);
        
    	try
    	{
        	configuration.load();
        	        		
            ConfigurationSettings.ENABLE_DEBUGGING = configuration.get(CATEGORY_GENERAL, ConfigurationSettings.ENABLE_DEBUGGING_CONFIGNAME, ConfigurationSettings.ENABLE_DEBUGGING_DEFAULT).getBoolean(ConfigurationSettings.ENABLE_DEBUGGING_DEFAULT);  
        }
        catch (Exception e)
        {
            FMLLog.log(Level.SEVERE, e, Reference.MOD_NAME + " has had a problem loading its configuration");
        }
        finally
        {
            configuration.save();
        }
    }

    public static void set(String categoryName, String propertyName, String newValue)
    {
        configuration.load();
        
        if (configuration.getCategoryNames().contains(categoryName))
        {
            if (configuration.getCategory(categoryName).containsKey(propertyName))
            {
                configuration.getCategory(categoryName).get(propertyName).set(newValue);
            }
        }
        
        configuration.save();
    }
}