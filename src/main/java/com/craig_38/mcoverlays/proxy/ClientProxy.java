package com.craig_38.mcoverlays.proxy;

import com.craig_38.mcoverlays.MCOverlays;

import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;


public class ClientProxy extends CommonProxy
{

    @Override
    public void registerRenderTickHandler()
    {
        TickRegistry.registerTickHandler(MCOverlays.instance.overlayHandler, Side.CLIENT);
    }

}
