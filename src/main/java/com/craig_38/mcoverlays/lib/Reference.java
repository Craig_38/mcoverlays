package com.craig_38.mcoverlays.lib;

public final class Reference
{
    public static final String MOD_ID       = "MCOverlays";
    public static final String MOD_NAME     = "Minecraft Overlays";
    public static final String VERSION      = "@VERSION@";

    public static final String COMMON_PROXY = "com.craig_38.mcoverlays.proxy.CommonProxy";
    public static final String CLIENT_PROXY = "com.craig_38.mcoverlays.proxy.ClientProxy";
    
    public static final String NETWORK_CHANNEL = "OverlayNet";
}
