package com.craig_38.mcoverlays.overlay.template;

import com.craig_38.mcoverlays.overlay.Overlay;

public abstract class OverlayTimed extends Overlay
{
	protected long triggerTime = 0;
	
	public OverlayTimed(String name)
	{
		super(name);
		// TODO Auto-generated constructor stub
	}

	protected void Trigger()
	{
		super.Trigger();
		
		triggerTime = System.currentTimeMillis();
	}
}
