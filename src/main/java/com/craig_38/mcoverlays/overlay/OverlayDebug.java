package com.craig_38.mcoverlays.overlay;

import java.util.List;

import net.minecraft.client.gui.FontRenderer;

import com.craig_38.mcoverlays.MCOverlays;

public class OverlayDebug extends Overlay
{
	FontRenderer fontRenderer = minecraft.fontRenderer;
	
	public OverlayDebug(String name)
	{
		super(name);
	}

	@Override
	public void Render()
	{
		String activeOverlays = Integer.toString(MCOverlays.instance.overlayHandler.GetActiveOverlays().size());
		String allOverlays = Integer.toString(MCOverlays.instance.overlayHandler.GetAllOverlays().size());
		
		String s = "Active Overlays: (" + activeOverlays + " / " + allOverlays + ")";
		
		List<Overlay> activeOverlaysList = MCOverlays.instance.overlayHandler.GetActiveOverlays();
		
		if (!minecraft.gameSettings.showDebugInfo && minecraft.inGameHasFocus)
		{
			fontRenderer.drawStringWithShadow("Overlay debug panel", 2, 2, 0xFFFFFFFF);
			fontRenderer.drawStringWithShadow(s, 2, 22, 0xFFFFFFFF);
			
			for (int i = 0; i <= activeOverlaysList.size() - 1 ; i++)
			{
				fontRenderer.drawStringWithShadow(activeOverlaysList.get(i).GetName(), 2, 42 + (10 * i), 0xFFFFFFFF);
			}
		}
	}
}
