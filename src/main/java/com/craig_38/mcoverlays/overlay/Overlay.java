package com.craig_38.mcoverlays.overlay;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import cpw.mods.fml.client.FMLClientHandler;

public abstract class Overlay
{
	private String overlayName;
	
	public boolean active = true;
	protected boolean triggered = false;
	
	protected final Minecraft minecraft;
	protected ScaledResolution scaledresolution;
	
	protected int screenWidth;
	protected int screenHeight;
	
	public Overlay(String name)
	{
		this.overlayName = name;
		
		minecraft = FMLClientHandler.instance().getClient();
	}
	
	public abstract void Render();
			
	public String GetName()
	{
		return this.overlayName;
	}
	
	public void UpdateVariables()
	{
		scaledresolution = new ScaledResolution(minecraft.gameSettings, minecraft.displayWidth, minecraft.displayHeight);
		screenWidth = scaledresolution.getScaledWidth();
		screenHeight = scaledresolution.getScaledHeight();
	}
	
	public void KeyTyped(char character, int keyCode)
	{
		
	}
	
	protected void Trigger()
	{
		triggered = true;
	}
}
