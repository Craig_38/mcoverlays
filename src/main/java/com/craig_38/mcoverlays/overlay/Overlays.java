package com.craig_38.mcoverlays.overlay;

import com.craig_38.mcoverlays.MCOverlays;
import com.craig_38.mcoverlays.configuration.ConfigurationSettings;

public class Overlays
{
	public static final OverlayDebug debug = new OverlayDebug("debug");
	
    public static void init()
    {
    	if (ConfigurationSettings.ENABLE_DEBUGGING)
    	{
    		MCOverlays.instance.overlayHandler.RegisterOverlay(debug);
    	}
    }
}
